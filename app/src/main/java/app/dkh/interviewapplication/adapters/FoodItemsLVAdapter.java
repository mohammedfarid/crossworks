package app.dkh.interviewapplication.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.dkh.interviewapplication.R;
import app.dkh.interviewapplication.models.Item;
import io.realm.RealmBaseAdapter;
import io.realm.RealmList;

public class FoodItemsLVAdapter extends RealmBaseAdapter implements ListAdapter {

    private Context mContext;
    private RealmList<Item> mItemRealmList;

    private static class ViewHolder {
        TextView mNameItemTv;
        ImageView mPhotoItemTv;
    }

    public FoodItemsLVAdapter(Context context, RealmList<Item> realmResults) {
        super(realmResults);
        mContext = context;
        mItemRealmList = realmResults;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.view_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.mNameItemTv = convertView.findViewById(R.id.name_item_menu);
            viewHolder.mPhotoItemTv = convertView.findViewById(R.id.photo_item_menu);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Item item = mItemRealmList.get(position);
        viewHolder.mNameItemTv.setText(item.getName());

        Picasso.get()
                .load(Uri.parse(item.getPhotoUrl()))
                .placeholder(R.drawable.ic_block_black_24dp)
                .into(viewHolder.mPhotoItemTv);

        return convertView;
    }


}
