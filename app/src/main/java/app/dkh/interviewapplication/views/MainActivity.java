package app.dkh.interviewapplication.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import app.dkh.interviewapplication.R;
import app.dkh.interviewapplication.adapters.FoodItemsLVAdapter;
import app.dkh.interviewapplication.models.Item;
import app.dkh.interviewapplication.viewmodels.MainActivityViewModel;
import io.realm.RealmList;


public class MainActivity extends AppCompatActivity {

    // TODO: Implement the views. You decide whether you want to use fragments, activities or
    // something else for displaying the content of the app
    public static final String ITEM_DATA = "ItemData";

    private ListView mMenuList;
    private FoodItemsLVAdapter mFoodItemsLVAdapter;
    private ProgressDialog mProgressDialog;
    private RealmList<Item> mItemRealmList;

    private MainActivityViewModel mMainActivityViewModel;


    // TODO: Feel free to restructure the classes (add more folders, classes, move classes)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (mMainActivityViewModel == null)
            mMainActivityViewModel = new MainActivityViewModel(this);

        mProgressDialog = new ProgressDialog(MainActivity.this, R.style.AppCompatProgressDialogStyle);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);

        mMenuList = findViewById(R.id.list_item);

        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemFragment itemFragment = new ItemFragment();

                Bundle bundle = new Bundle();
                bundle.putInt(ITEM_DATA, position);
                itemFragment.setArguments(bundle);

                replaceFragment(itemFragment);

            }
        });

        mMainActivityViewModel.insertMenuItem(this);
    }

    public void progressDialogHide() {
        mProgressDialog.cancel();
    }

    public void progressDialogShow() {
        mProgressDialog.show();
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void initList(RealmList<Item> realmResults) {
        mFoodItemsLVAdapter = new FoodItemsLVAdapter(this, realmResults);
        mItemRealmList = realmResults;
        mMenuList.setAdapter(mFoodItemsLVAdapter);
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.main_frame_layout, fragment, "").commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
