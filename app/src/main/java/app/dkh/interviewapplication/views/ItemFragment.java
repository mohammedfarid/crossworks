package app.dkh.interviewapplication.views;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.dkh.interviewapplication.R;
import app.dkh.interviewapplication.models.Item;
import app.dkh.interviewapplication.viewmodels.ItemFragmentViewModel;

public class ItemFragment extends Fragment {

    private ImageView mPhotoItem;
    private TextView mNameItem;
    private TextView mDescItem;
    private Item mItem;

    private int position;

    private ItemFragmentViewModel mItemFragmentViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mItemFragmentViewModel == null)
            mItemFragmentViewModel = new ItemFragmentViewModel(getActivity());
        if (getArguments() != null) {
            position = getArguments().getInt(MainActivity.ITEM_DATA);
            mItem = mItemFragmentViewModel.getItem(position);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_item, container, false);
        mPhotoItem = view.findViewById(R.id.photo_iv);
        mNameItem = view.findViewById(R.id.name_tv);
        mDescItem = view.findViewById(R.id.desc_tv);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNameItem.setText(mItem.getName());
        mDescItem.setText(mItem.getDescription());
        Picasso.get()
                .load(Uri.parse(mItem.getPhotoUrl()))
                .placeholder(R.drawable.ic_block_black_24dp)
                .into(mPhotoItem);
    }
}
