package app.dkh.interviewapplication;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {
    public static final String API_BASE_URL = "https://api.myjson.com/bins/";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration mRealmConfiguration = new RealmConfiguration.Builder()
                .name("menu_item.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(mRealmConfiguration);
    }
}
