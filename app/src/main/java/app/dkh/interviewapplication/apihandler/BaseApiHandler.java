package app.dkh.interviewapplication.apihandler;


import java.util.concurrent.TimeUnit;

import app.dkh.interviewapplication.MyApplication;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by usear on 5/24/2016.
 */
public class BaseApiHandler {

    private static final int TIMEOUT_MIN = 3;
    private static Retrofit retrofit = null;

    public static Retrofit setupBaseApi() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_MIN, TimeUnit.MINUTES)
                    .readTimeout(TIMEOUT_MIN, TimeUnit.MINUTES)
                    .addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

}
