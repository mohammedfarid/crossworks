package app.dkh.interviewapplication.apihandler;




import java.util.List;

import app.dkh.interviewapplication.models.FoodItem;
import retrofit2.Call;
import retrofit2.http.GET;


public interface BaseApi {
    //Json Data
    @GET("kvdzh")
    Call<FoodItem> getFoodItems();

}
