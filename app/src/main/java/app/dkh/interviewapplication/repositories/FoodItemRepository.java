package app.dkh.interviewapplication.repositories;

import app.dkh.interviewapplication.models.FoodItem;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class FoodItemRepository {
    // TODO: return all food items

    private Realm _realm;

    public FoodItemRepository(Realm realm) {
        _realm = realm;
    }

    public RealmResults<FoodItem> getFoodItems() {
        if (_realm == null || _realm.isClosed())
            _realm = Realm.getDefaultInstance();
        return _realm.where(FoodItem.class).findAll();
    }
    public  RealmResults<FoodItem> getFoodItemsDatabyId(int id) {
        if (_realm == null || _realm.isClosed())
            _realm = Realm.getDefaultInstance();
        return _realm.where(FoodItem.class).equalTo("id",id).findAll();
    }
}
