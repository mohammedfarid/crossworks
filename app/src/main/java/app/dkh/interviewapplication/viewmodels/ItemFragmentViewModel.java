package app.dkh.interviewapplication.viewmodels;

import android.content.Context;

import app.dkh.interviewapplication.apihandler.BaseApi;
import app.dkh.interviewapplication.apihandler.BaseApiHandler;
import app.dkh.interviewapplication.models.FoodItem;
import app.dkh.interviewapplication.models.Item;
import app.dkh.interviewapplication.repositories.FoodItemRepository;
import app.dkh.interviewapplication.service.Utils;
import app.dkh.interviewapplication.views.MainActivity;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemFragmentViewModel extends RealmViewModel {

    private Realm mRealm;

    private FoodItem mFoodItem;

    private RealmResults<FoodItem> mFoodItemRealmResults;

    private FoodItemRepository mFoodItemRepository;


    public ItemFragmentViewModel(Context mContext) {
        super();
        mRealm = getRealm();
        if (mFoodItemRepository == null)
            mFoodItemRepository = new FoodItemRepository(mRealm);
    }

    public Item getItem(Integer id){

        mFoodItemRealmResults = mFoodItemRepository.getFoodItems();

        FoodItem foodItem = mFoodItemRealmResults.get(0);
        RealmList<Item> item = foodItem.getItems();
        return item.get(id);
    }
}
