package app.dkh.interviewapplication.viewmodels;

import android.content.Context;

import app.dkh.interviewapplication.apihandler.BaseApi;
import app.dkh.interviewapplication.apihandler.BaseApiHandler;
import app.dkh.interviewapplication.models.FoodItem;
import app.dkh.interviewapplication.models.Item;
import app.dkh.interviewapplication.repositories.FoodItemRepository;
import app.dkh.interviewapplication.service.Utils;
import app.dkh.interviewapplication.views.MainActivity;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityViewModel extends RealmViewModel {

    private Realm mRealm;

    private BaseApi mServiceApi;
    private Call<FoodItem> mFoodItemCall;

    private FoodItem mFoodItem;

    private RealmResults<FoodItem> mFoodItemRealmResults;

    private FoodItemRepository mFoodItemRepository;

    private MainActivity mMainActivity;

    public MainActivityViewModel(Context mContext) {
        super();
        mRealm = getRealm();
        if (mServiceApi == null)
            mServiceApi = BaseApiHandler.setupBaseApi().create(BaseApi.class);
        if (mMainActivity == null)
            mMainActivity = (MainActivity) mContext;
        if (mFoodItemRepository == null)
            mFoodItemRepository = new FoodItemRepository(mRealm);
    }

    public void insertMenuItem(Context mContext) {
        mFoodItemCall = mServiceApi.getFoodItems();
        if (Utils.isConnectionOn(mContext)) {
            mMainActivity.progressDialogShow();
            mFoodItemCall.enqueue(new Callback<FoodItem>() {
                @Override
                public void onResponse(Call<FoodItem> call, Response<FoodItem> response) {
                    if (response.isSuccessful()) {
                        try {
                            mFoodItem = response.body();

                            mRealm.beginTransaction();
                            mRealm.copyToRealmOrUpdate(mFoodItem);
                            mRealm.commitTransaction();

                            mFoodItemRealmResults = mFoodItemRepository.getFoodItems();

                            FoodItem foodItem = mFoodItemRealmResults.get(0);
                            RealmList<Item> item = foodItem.getItems();

                            mMainActivity.initList(item);

                            mMainActivity.progressDialogHide();


                        } catch (Exception e) {
                            mMainActivity.progressDialogHide();
                            mMainActivity.toastMessage("Catch Problem :" + e.getMessage());
                        }
                    } else {
                        mMainActivity.progressDialogHide();
                        mMainActivity.toastMessage("Response Problem :" + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<FoodItem> call, Throwable t) {
                    mMainActivity.progressDialogHide();
                    mMainActivity.toastMessage("Service Problem :" + t.getMessage());
                }
            });
        } else {
            mMainActivity.progressDialogHide();
            mMainActivity.toastMessage("Network Offline");
        }
    }
}
